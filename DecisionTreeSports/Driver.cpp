
#include <iostream>
#include <fstream>
#include <iomanip>
#include "decisionTreeSports.h"

using namespace std;


int main()
{

	// Create decision tree
	decisionTreeSports treeTrial;

	// File names
	char inputFileName[] = "foul_opp_double_you_bonus.txt"; // If inputting from file
	char echoFileName[] = "trial.txt"; // If defining manually and echoing to file

	// Define decision tree
	ifstream inputDevice(inputFileName); 
	ofstream echoFile(echoFileName);
	treeTrial.setDecisionTree(inputDevice);  // Use inputDevice if definition already created
										  // Use echoFile if entering new definition manually
	
	// Define values for symbolic variables used in decision tree definition
	treeTrial.addRow("fg%", 0.44);
	treeTrial.addRow("foul%", 0.1);
	treeTrial.addRow("tfg%", 0.34);
	treeTrial.addRow("ft%", 0.7);
	treeTrial.probabilityResults();

	// Generate output for parametric analysis for decision tree
	ofstream resultsFile("results.txt");
	resultsFile << fixed << setprecision(3);

	// Generate results as a function of the factor relative to FG% and 3FG%
	for (double fact = 0.45; fact <= 1.00; fact += 0.01)
	{
		treeTrial.modifyRow("fg%", fact*0.44);
		treeTrial.modifyRow("tfg%", fact*0.34);
		treeTrial.probabilityResults();
		resultsFile << fact << " ";
		resultsFile << treeTrial;
	}

	// Close files
	echoFile.close();
	inputDevice.close();
	resultsFile.close();
	
	return 0;
}