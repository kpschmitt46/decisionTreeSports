
#include <string>
#include <iostream>
#include <stdexcept>

#include "probabilityTable.h"

using namespace std;

// Constructor for probability table
probabilityTable::probabilityTable()
{
	length = 0;
	clearTable();
}

// Query user to build probability table
void probabilityTable::setTable()
{

	// Allocate internals
	int iEntry = 0;
	string nameTemp = "";
	double valueTemp = 0.0;

	// Initial query for name
	cout << "Constructing probability table..." << endl;
	cout << "Hit enter without probability name to signal completion" << endl;
	cout << endl;
	cout << "Enter probability name: ";
	getline(cin, nameTemp);

	// Query user for table entries until complete
	for (iEntry = 0; iEntry < nProbMax; iEntry++)
	{

		// Once user specifies done, auto-initialize remainder of table
		if (nameTemp == "")
		{
			name[iEntry] = "";
			value[iEntry] = 0.0;
			continue;
		}

		// Query for value
		cout << "Enter probability value for " << nameTemp << ": ";
		cin >> valueTemp;
		while (valueTemp < 0.0 || valueTemp > 1.0)
		{
			cout << "Probability value must be between 0 and 1.  Try again: ";
			cin >> valueTemp;
		}
		// Fill row
		addRow(nameTemp, valueTemp);
		cout << endl;

		// Query for name
		cout << "Enter probability name: ";
		cin.ignore(); // ignore trailing \n
		getline(cin, nameTemp);
	}

}

// Clears probability table
void probabilityTable::clearTable()
{
	length = 0;
	for (size_t i = 0; i < length; i++)
	{
		name[i] = "";
		value[i] = 0.0;
	}
}

// Adds row to probability table
void probabilityTable::addRow(const std::string key, const double newValue)
{
	try
	{
		name[length] = key;
		value[length] = newValue;
		length += 1;
	}
	catch (...)
	{
		cout << "Probability table exceeds " << nProbMax;
	}
	
}

// Modifies row of probability table
void probabilityTable::modifyRow(const std::string key, const double newValue)
{
	// find key and output value
	for (size_t i = 0; i < length; i++)
	{
		if (name[i] == key)
		{
			value[i] = newValue;
			return;
		}
	}

	// key value not in table
	return;

}

// Looks up value from table
double probabilityTable::lookup(const string key)
{

	// find key and output value
	for (size_t i = 0; i < length; i++)
	{
		if (name[i] == key)
			return value[i];
	}

	// value not found in table
	double valueTemp;
	cout << "Enter probability value for " << key << ": ";
	cin >> valueTemp;
	while (valueTemp < 0.0 || valueTemp > 1.0)
	{
		cout << "Probability value must be between 0 and 1.  Try again: ";
		cin >> valueTemp;
	}
	// Fill row
	addRow(key, valueTemp);

	return valueTemp;

}

