
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>

#include "decisionTreeSports.h"

using namespace std;

// Initialize result options
const string decisionTreeSports::RESULTS[3] = { "win", "tie", "lose" };

// Constructor for decision tree
decisionTreeSports::decisionTreeSports() :
	probTable()
{

	for (int i = 0; i < nBranchMax; i++)
	{
		decisionTree[i].name = "";
		decisionTree[i].result = ""; 
		decisionTree[i].probability = ""; 
		decisionTree[i].parentIndex = -1;
	}

	int nBranchTotal = 0;
	probResult[0] = 0.0;
	probResult[1] = 0.0;
	probResult[2] = 0.0;

}

// 
void printInstructions()
{
	cout << endl << endl;
	cout << "Constructing decision tree..." << endl;
	cout << endl;
	cout << "Instructions:" << endl;
	cout << endl;
	cout << "For each node, the user will be prompted to enter the number branches." << endl;
	cout << "   To continue building from the node, the user enters a number." << endl;
	cout << "   The user will then be prompted to define each branch." << endl;
	cout << "      The branch description should be a pithy description of the branch." << endl;
	cout << "      The branch probability may be a numerical value or a symbolic expression." << endl;
	cout << "   To end the branch, the user enters one of the results win, tie, or lose." << endl;
	cout << endl;
	cout << "The decision tree construction will continue until all branches are ended." << endl;
	cout << endl;
}

// Query user to generate decision tree
void decisionTreeSports::setDecisionTree(istream& inputDevice)
{

	int iCurrent = 0;
	int iOpen = 1;

	// initialize base of tree (values not used)
	decisionTree[0].name = "base";
	decisionTree[0].result = "";
	decisionTree[0].probability = "1.0";
	decisionTree[0].parentIndex = -1;

	printInstructions();

	// recurisve construction of tree from root
	recurionBuild(iCurrent,iOpen, inputDevice, cout);
	cout << endl;
}

void decisionTreeSports::setDecisionTree(std::ostream& echoFile)
{

	int iCurrent = 0;
	int iOpen = 1;

	// initialize base of tree (values not used)
	decisionTree[0].name = "base";
	decisionTree[0].result = "";
	decisionTree[0].probability = "1.0";
	decisionTree[0].parentIndex = -1;

	printInstructions();

	// recurisve construction of tree from root
	recurionBuild(iCurrent, iOpen, cin, echoFile);
	cout << endl;
}

// Recursively build decision tree
void decisionTreeSports::recurionBuild(int iParent, int& iOpen, istream& inputDevice, std::ostream& echoFile)
{

	// Allocate internals
	string branchDescription;
	char nodeDecision[5];
	int nBranchCurrent;
	
	// Initial query to user
	cout << "Input number of branches from root of tree";
	{int iCurrent = iParent;
	int iCurentPrev = 0; // by convention, set iCurrentPrev to 0
	recursionPrintBranches(iCurrent, iCurentPrev); }
	cout << ": ";
	inputEcho<char[5]>(nodeDecision, inputDevice, echoFile);

	if (islower(nodeDecision[0]))
	{
		decisionTree[iParent].result = nodeDecision;
		return;
	}
	else
		nBranchCurrent = nodeDecision[0] - '0';  // convert c-string to int

	// Add branches recursively
	string probabilityOther = "";
	for (int i = 0; i < nBranchCurrent; i++)
	{
		// Set branch name
		cout << "Input branch description: ";
		inputEcho<string>(decisionTree[iOpen + i].name, inputDevice, echoFile);

		// Set branch probability
		if (i < nBranchCurrent-1)  // not last branch from node
		{
			cout << "Input branch probability: ";
			inputEcho<string>(decisionTree[iOpen + i].probability, inputDevice, echoFile);
			if (i > 0) probabilityOther += "-";
			probabilityOther += "(" + decisionTree[iOpen + i].probability + ")";
		}
		else // last branch from node
		{
			// Automate probability of last branch
			cout << "Infer branch probability inferred: ";
			decisionTree[iOpen + i].probability = "1.0-" + probabilityOther;
			cout << decisionTree[iOpen + i].probability << endl << endl;
		}

		// Set branch parent
		decisionTree[iOpen + i].parentIndex = iParent;
	}

	// Recursively call script for next set of branches
	int iOpenPrev = iOpen;
	iOpen += nBranchCurrent; // update next open index
	for (int i = 0; i < nBranchCurrent; i++)
	{
		recurionBuild(iOpenPrev + i, iOpen, inputDevice, echoFile);
	}

	// Save number of branches
	nBranchTotal = iOpen;

}

// Recursively print all parent branches to input branch
void decisionTreeSports::recursionPrintBranches(int &iCurrent, int iCurrentPrev)
{
	iCurrentPrev = iCurrent;

	if (iCurrent != 0)  // keep penetrating
	{
		recursionPrintBranches(decisionTree[iCurrent].parentIndex, iCurrentPrev);
	}

	if (iCurrentPrev != 0) // once penetrated, follow branches back out to print names
		cout << "-" << decisionTree[iCurrentPrev].name;

}


// Recursively multiple probabilities of decision tree path
void decisionTreeSports::recursionMultProb(int iCurrent, double &probCurrent)
{
	double probInc = convertSymbolic(decisionTree[iCurrent].probability);
	if (probInc >= 0.0 && probInc <= 1.0)
		probCurrent *= probInc;
	else
	{
		cout << "Incremental probability is less than 0.0 or greater than 1.0." << endl;
		probCurrent = NAN;
	}
		
	if (iCurrent != 0)  // keep penetrating
	{
		recursionMultProb(decisionTree[iCurrent].parentIndex, probCurrent);
	}
}

// Convert symbol expression to value
double decisionTreeSports::convertSymbolic(string Exp)
{

	double callStack[100] = { 0.0 }; // array with encoded operations
	int k = 0; // index in call stack

			   // operator codes
	const double MUL = -444;
	const double DIV = -555;
	const double ADD = -666;
	const double SUB = -777;

	// encode call stack
	size_t i = 0; // index in symbolic expression
	while (i < Exp.length())
	{
		int index = static_cast<int>(Exp[i]);
		if (index == 40) // left parantheses
		{
			int j = 1;
			int nPairs = 1;
			index = static_cast<int>(Exp[i + j]);
			while (index != 41 || nPairs != 1) // get length of parantheses
			{
				if (index == 40)
					nPairs++;
				if (index == 41)
					nPairs--;
				j++;
				if (i + j == Exp.length())
				{
					cout << "Parantheses mismatched." << endl;
					return NAN;
				}
				index = static_cast<int>(Exp[i + j]);
			}
			callStack[k++] = convertSymbolic(Exp.substr(i + 1, j - 1));
			i += j + 1;
		}
		else if ((index >= 48) && (index <= 57)) // start of number
		{
			int j = 0;
			index = static_cast<int>(Exp[i + j]);
			while (index == 46 || ((index >= 48) && (index <= 57))) // get length of number
			{
				j++;
				if (i + j == Exp.length())
					break;
				index = static_cast<int>(Exp[i + j]);
			}
			if (index == 40)
			{
				cout << "Operator must occur between symbol and left parantheses." << endl;
				return NAN;
			}
			callStack[k++] = atof(Exp.substr(i, j).c_str()); // returns c string as double
			i += j;
		}
		else if ((index >= 97) && (index <= 122)) // start of symbol
		{
			int j = 0;
			index = static_cast<int>(Exp[i + j]);
			while (index != 40 && index != 41 && index != 42 && index != 43 && index != 45 && index != 47) // get length of symbol
			{
				j++;
				if (i + j == Exp.length())
					break;
				index = static_cast<int>(Exp[i + j]);
			}
			if (index == 40)
			{
				cout << "Operator must occur between symbol and left parantheses." << endl;
				return NAN;
			}
			callStack[k++] = probTable.lookup(Exp.substr(i, j));
			i += j;
		}
		else if (index == 42) // multiply
		{
			callStack[k++] = MUL;
			i++;
		}
		else if (index == 47) // divide
		{
			callStack[k++] = DIV;
			i++;
		}
		else if (index == 43) // add
		{
			callStack[k++] = ADD;
			i++;
		}
		else if (index == 45) // subtract
		{
			callStack[k++] = SUB;
			i++;
		}
		else if (index == 32) // space
		{
			i++;
		}
		else
		{
			if (index == 41)
				cout << "Parantheses mismatched." << endl;
			else
				cout << "Illegal symbol used in probability expression." << endl;
			return NAN;
		}
	}

	// evaluate MULT and DIV in call stack
	int stackLen = k;
	int signTracker = 1;
	for (int k = 0; k < stackLen; k++)
	{
		if (callStack[k] == MUL)
		{
			callStack[k + 1] = signTracker*(callStack[k - 1] * callStack[k + 1]);
			callStack[k] = ADD;
			callStack[k - 1] = 0.0;
			signTracker = 1;
			k++;
		}
		else if (callStack[k] == DIV)
		{
			callStack[k + 1] = signTracker*(callStack[k - 1] / callStack[k + 1]);
			callStack[k] = ADD;
			callStack[k - 1] = 0.0;
			signTracker = 1;
			k++;
		}
		else
		{
			if (callStack[k] == ADD)
				signTracker = 1;
			else if (callStack[k] == SUB)
				signTracker = -1;
		} // callStack[k] = callStack[k];

	}

	// evaluate ADD and SUB in call stack
	for (int k = 0; k < stackLen; k++)
	{
		if (callStack[k] == ADD)
		{
			if (k == 0)
			{
				callStack[k] = 0.0;
			}
			else
			{
				callStack[k + 1] = callStack[k - 1] + callStack[k + 1];
				callStack[k] = 0.0;
				callStack[k - 1] = 0.0;

			}
			k++;
		}
		else if (callStack[k] == SUB)
		{
			if (k == 0)
			{
				callStack[k] = 0.0;
				callStack[k + 1] = -callStack[k + 1];
			}
			else
			{
				callStack[k + 1] = callStack[k - 1] - callStack[k + 1];
				callStack[k] = 0.0;
				callStack[k - 1] = 0.0;
			}
			k++;
		}
		else {} //	callStack[kk] = callStack[k];

	}

	return callStack[k - 1];

}

// Calculate probability associated with each result (win, tie, lose)
void decisionTreeSports::probabilityResults()
{

	// calculate total probability for each result
	for (int i = 0; i < 3; i++)
	{
		probResult[i] = 0.0;

		// loop through each branch
		for (int j = 0; j < nBranchTotal; j++)
		{
			
			if (decisionTree[j].result==RESULTS[i])  // branch matches result
			{
				double probInc = 1.0;
				recursionMultProb(j, probInc);	// recursively multiply probabilities of parent branches
				probResult[i] += probInc;
			}
		}

	}

}

// Set probability table associated with decision tree
void decisionTreeSports::setTable()
{
	probTable.setTable();
}

// Adds row to probability table associated with decision tree
void decisionTreeSports::addRow(std::string key, double newValue)
{
	probTable.addRow(key, newValue);
}

// Modify probability table associated with decision tree
void decisionTreeSports::modifyRow(std::string key, double newValue)
{
	probTable.modifyRow(key, newValue);
}

ostream& operator<<(ostream& osObject, const decisionTreeSports& cObject)
{
	osObject << cObject.probResult[0] << " ";
	osObject << cObject.probResult[1] << " ";
	osObject << cObject.probResult[2] << endl;

	return osObject;

}