#pragma once

#ifndef h_decisionTreeSports
#define h_decisionTreeSports

#include <string>
#include <fstream>

#include "probabilityTable.h"

// Branch definition
struct Branch
{
	std::string name;		 // Description of branch
	std::string result;      // Result of branch (win, lose, tie)
	std::string probability; // Probability of branch (symbolic)
	int parentIndex;		 // Index of branch parent
};

class decisionTreeSports
{
	// Output probability results
	friend std::ostream& operator<<(std::ostream&, const decisionTreeSports&);

public:
	// Default constructor
	decisionTreeSports();

	// Queries user or input file to develop decision tree
	void setDecisionTree(std::istream& inputDevice); // accept input from file
	void setDecisionTree(std::ostream& echoFile);    // accept input from user, echo input to file

	// Queries user to develop probability table
	void setTable();

	// Adds row to probability table
	// input 1 == symbol name
	// input 2 == value for symbol
	void addRow(std::string, double);

	// Modifies row of probability table
	// input 1 == symbol name
	// input 2 == new value for symbol
	void modifyRow(std::string, double);

	// Calculates probabilities for different results
	void probabilityResults();

private:
	// Size constraints
	static const int nBranchMax = 1000;

	// Array of possible results "win" "tie" and "lose"
	static const std::string RESULTS[3];

	// Data storage elements
	probabilityTable probTable;			// Probability look-up table
	Branch decisionTree[nBranchMax];    // Array of branches (tree)
	int nBranchTotal;					// Number of defined branches
	double probResult[3];				// Probabilities associated with each result

	// Used by setDecisionTree to generate tree structure
	void recurionBuild(int iCurrent, int& iOpen, std::istream& inputDevice, std::ostream& echoFile);

	// Used by setDecisionTree to print branch path to current node
	void recursionPrintBranches(int &iCurrent, int iCurrentPrev);

	// Used by probabilityResults to calculate probability for branch 
	// FORMAL EXCEPTION HANDLING FOR SYMBOLIC EXPRESSIONS NOT YET IMPLEMENTED
	void recursionMultProb(int iCurrent, double &probCurrent);

	// Used by recursionMultProb to convert symbolic equation to value
	double convertSymbolic(std::string symbolicExp);

	// Accept input and echo to appropriate device
	template <typename Type>
	void inputEcho(Type& x, std::istream& in, std::ostream& out) 
	{
		in >> x;
		if (&in == &cin)
			out << x << '\n';
		else
			cout << x << '\n';
	}

};

#endif