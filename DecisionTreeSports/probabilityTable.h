#pragma once

#ifndef h_probabilityTable
#define h_probabilityTable

#include <string>

class probabilityTable
{
public:
	// Default constructor
	probabilityTable();

	// Queries user to develop probability table
	void setTable();

	// Clears probability table
	void clearTable();

	// Adds row to probability table
	// input 1 == symbol name
	// input 2 == value for symbol
	void addRow(const std::string, const double);

	// Modifies row of probability table
	// input 1 == symbol name
	// input 2 == new value for symbol
	void modifyRow(const std::string, const double);

	// Looks up value from table
	// input 1 == symbol name for look-up
	double lookup(const std::string);

protected:

	// Constants
	static const int nProbMax = 100;

	// Data stroage elements
	size_t length;				// number of values in probability table
	std::string name[nProbMax]; // symbol names
	double value[nProbMax];		// symbol values
};

#endif